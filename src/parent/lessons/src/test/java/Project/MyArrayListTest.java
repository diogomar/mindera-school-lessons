package Project;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MyArrayListTest {

    @Test
    void size() {
    }

    @Test
    void isEmpty() {
        MyArrayList arrayList = new MyArrayList();
        assertTrue( arrayList.isEmpty() );

        arrayList.add( "" );
        assertFalse( arrayList.isEmpty() );
    }

    @Test
    void contains() {

    }

    @Test
    void iterator() {

    }

    @Test
    void toArray() {

    }

    @Test
    void toArray1() {

    }

    @Test
    void add() {
        MyArrayList arrayList = new MyArrayList();
        assertTrue( arrayList.add( " 1" ) );
    }

    @Test
    void remove() {
        MyArrayList arrayList = new MyArrayList();
        arrayList.add( 2 );
        arrayList.add( 3 );
        arrayList.add( 4 );
        assertTrue( arrayList.size() == 3 );
        arrayList.remove( 0 );
        assertTrue( arrayList.size() == 2 );

    }

    @Test
    void containsAll() {
    }

    @Test
    void addAll() {
    }

    @Test
    void addAll1() {
    }

    @Test
    void removeAll() {
    }

    @Test
    void retainAll() {
    }

    @Test
    void replaceAll() {
    }

    @Test
    void sort() {
    }

    @Test
    void clear() {
        MyArrayList arrayList = new MyArrayList();
        arrayList.add( "2" );
        assertTrue( arrayList.size() == 1 );
        arrayList.clear();
        assertTrue( arrayList.size() == 0 );
    }

    @Test
    void get() {
        MyArrayList arrayList = new MyArrayList();
        arrayList.add( 2 );
        assertTrue( arrayList.get( 0 ).equals( 2 ) );
    }

    @Test
    void set() {
    }

    @Test
    void add1() {
    }

    @Test
    void remove1() {
    }

    @Test
    void indexOf() {
    }

    @Test
    void lastIndexOf() {
    }

    @Test
    void listIterator() {
    }

    @Test
    void listIterator1() {
    }

    @Test
    void subList() {
    }

    @Test
    void spliterator() {
    }
}