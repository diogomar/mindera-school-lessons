package com.minderaschool.linkedObjects;

import org.junit.jupiter.api.Test;

class LinkObjectsTest {

    @Test
    void add() {
        Node one = new Node( "hello" );
        Node two = new Node( "lada" );
        LinkObjects x = new LinkObjects();
        x.add( one );
        x.add( two );
        assert (one.next != null);
        assert (one.next.text.equals( "lada" ));
    }

    @Test
    void addone() {
        Node one = new Node( "hello" );
        Node two = new Node( "lada" );
        Node tree = new Node( "da" );
        LinkObjects x = new LinkObjects();
        x.add( one );
        x.add( two );
        x.add( tree );

        assert (one.next != null);
        assert (two.next != null);
        assert (tree.next != null);
        assert (one.next.text.equals( "lada" ));
        assert (two.next.text.equals( "da" ));
        assert (tree.next.text.equals( "da" ));
    }

    @Test
    void remove() {
        Node one = new Node( "hello" );
        Node two = new Node( "lada" );
        Node tree = new Node( "pao" );

        LinkObjects x = new LinkObjects();
        x.add( two );
        x.add( tree );
        x.remove( 2 );
        assert (one.next != null);
        assert (two.next == null);
    }

    @Test
    void removeone() {
        Node one = new Node( "hello" );
        Node two = new Node( "lada" );
        Node tree = new Node( "pao" );

        LinkObjects x = new LinkObjects();
        x.add( one );
        x.add( two );
        x.add( tree );
        x.remove( 1 );
        assert (one.next.text.equals( "pao" ));
        assert (tree.next == null);

    }

    @Test
    void get() {
        Node one = new Node( "hello" );
        Node two = new Node( "lada" );
        Node tree = new Node( "pao" );
        LinkObjects x = new LinkObjects();
        x.add( two );
        x.add( tree );
        Node y = x.get( 0 );
        Node r = x.get( 1 );
        Node t = x.get( 2 );
        assert (y.text.equals( "hello" ));
        assert (r.text.equals( "lada" ));
        assert (t.text.equals( "pao" ));

    }


}
