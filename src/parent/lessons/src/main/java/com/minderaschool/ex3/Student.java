package com.minderaschool.ex3;

public class Student extends Person {

    private int numCourses;
    private String[] courses;
    private int[] grades;

    public Student(String name, String adress) {
        super(name, adress);
        numCourses = 0;
        courses = new String[5];
        grades = new int[5];
    }

    @Override
    public String toString() {
        return "Student: " + super.toString();
    }

    public void addCourseGrade(String course, int grade) {
        courses[numCourses] = course;
        grades[numCourses] = grade;
        numCourses++;
    }

    public void printGrades() {
        for (int i = 0; i < numCourses; i++) {
            System.out.println("Course:" + courses[i] + "| Grade" + grades[i]);
        }
    }

    public double getAverageGrade() {
        int sum = 0;
        for (int i = 0; i < numCourses; i++) {
            sum += grades[i];
        }
        return sum/numCourses;
    }
}
