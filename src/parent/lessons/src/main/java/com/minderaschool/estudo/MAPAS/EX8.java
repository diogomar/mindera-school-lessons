package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX8 {

    public static void main(String[] args) {

        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put( 1, "Red" );
        mapa.put( 2, "Green" );
        mapa.put( 3, "Black" );
        mapa.put( 4, "White" );
        mapa.put( 5, "Blue" );

        // print the map
        System.out.println( "The Original Map : " + mapa );

        System.out.println( "1. Is Key 'Green' exists? " );

        if ( mapa.containsValue( "Green" ) ) {
            //key exists
            System.out.println( "Yes! - " + mapa.get( "Green" ) );
        } else {
            //key does not exists
            System.out.println( "No!" );
        }

        System.out.println( "\n2. Is key 'Orange' exists?" );
        if ( mapa.containsValue( "Orange" ) ) {
            //key exists
            System.out.println( "Yes! - " + mapa.get( "Orange" ) );
        } else {
            //key does not exists
            System.out.println( "No!" );
        }
    }
}
