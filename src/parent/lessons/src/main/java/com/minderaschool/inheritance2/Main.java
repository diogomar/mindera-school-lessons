package com.minderaschool.inheritance2;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Figure> lista= new ArrayList<Figure>();
        lista.add(new Square(5));
        lista.add(new Circle(4));
        lista.add(new Triangule(4,3));

        for(int i = 0; i<lista.size();i++){
            lista.get(i).WhoAmI();
            System.out.println(lista.get(i).area());
        }
    }
}
