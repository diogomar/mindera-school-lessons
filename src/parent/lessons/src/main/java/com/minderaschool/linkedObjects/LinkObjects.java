package com.minderaschool.linkedObjects;

public class LinkObjects {

    public Node head;
    //public Node tail;
    public int length;

    public LinkObjects() {
    }

    public void add(Node head) {
        Node temp = head;

        while (temp.next != null) {
            temp.next = temp;
        }
        temp = temp.next;
    }

    public void remove(int index) {
        Node temp = head;
        Node temp2 = head.next;

        for (int i = 0; i < index - 1; i++) {
            temp = temp.next;
            temp2 = temp2.next;
        }

        temp.next = temp2.next;
    }

    public Node get(int index) {
        Node temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp;
    }

}
