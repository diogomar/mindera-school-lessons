package com.minderaschool.EvenOrOdd;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in );

        System.out.println( "Even or Odd Calculator !!!" );
        System.out.println( "========================" );
        System.out.println( "Insert the number you desire:" );
        int number = scanner.nextInt();

        if ( number % 2 == 0 ) {
            System.out.println( "True" );
        } else {
            System.out.println( "False" );
        }


    }
}
