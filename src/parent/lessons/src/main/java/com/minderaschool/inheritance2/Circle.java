package com.minderaschool.inheritance2;

public class Circle extends Figure {

    public Circle(double radius) {
        super.areaValue = Math.PI * (radius*radius);
        super.iAm = "Circle";
    }

    public double area() {
        return super.areaValue;
    }

    public void WhoAmI() {
        System.out.println(super.iAm);
    }

}
