package com.minderaschool.inheritance;

public class Rectangule extends Figure {

    public Rectangule(double h, double w) {
        super.areaValue = h * w;
        super.IAm = "Rectangule";
    }

    public double area() {
        return areaValue;
    }

    public void WhoAmI() {
        System.out.println(IAm);
    }

    public Rectangule(){

    }
}
