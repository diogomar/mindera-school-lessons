package com.minderaschool.inheritance2;

public class Square extends Figure {

    public Square(double side) {
        super.areaValue = side * side;
        super.iAm="Square";
    }

    public double area (){
        return areaValue;
    }
    public void WhoAmI(){
        System.out.println(iAm);
    }
}
