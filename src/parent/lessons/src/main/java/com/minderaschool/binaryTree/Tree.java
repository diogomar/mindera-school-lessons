package com.minderaschool.binaryTree;

public class Tree {

    private NodeTree root;

    public Tree() {
        this.root = null;
    }

    public Tree(NodeTree root) {
        this.root = root;
    }

    public NodeTree getRoot() {
        return root;
    }

    public void insertRoot(char element) {
        this.root = new NodeTree( element );
    }

    public void insertLeft(NodeTree node, char element) {
        if ( node.getLeft() == null ) {
            node.setLeft( new NodeTree( element ) );
        } else {
            insertLeft( node.getLeft(), element );
        }

    }

    public void insertRight(NodeTree node, char element) {
        if ( node.getRight() == null ) {
            node.setRight( new NodeTree( element ) );
        } else {
            insertRight( node.getRight(), element );
        }

    }


}
