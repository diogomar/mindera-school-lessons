// Write a Java program to get a shallow copy of a HashMap instance

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX6 {

    public static void main(String[] args) {

        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put( 1, "Red  " );
        mapa.put( 2, "Green" );
        mapa.put( 3, "Black" );
        mapa.put( 4, "White" );
        mapa.put( 5, "Blue " );

        // Print map

        System.out.println( "The Original Map: " + mapa );

        HashMap<Integer, String> mapa2 = new HashMap<>();

        mapa2 = (HashMap) mapa.clone();

        System.out.println("The Cloned Map: " + mapa2);

    }

}

