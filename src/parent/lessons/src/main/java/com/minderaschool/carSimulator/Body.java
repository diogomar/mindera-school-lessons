package com.minderaschool.carSimulator;

public class Body {

    private Door[] doors;

    public Body(int numDoor) {
        this.doors = new Door[numDoor];
        for (int i = 0; i < doors.length; i++) {
            doors[i] = new Door();
        }
    }

    public void open(int numDoor) {
        this.doors[numDoor].open();
    }

    public void close(int numDoor) {
        this.doors[numDoor].close();
    }

    public boolean isOpen(int numDoor) {

        return this.doors[numDoor].isOpen();

    }
}
