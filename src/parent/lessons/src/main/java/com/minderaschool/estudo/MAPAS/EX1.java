// Write a Java program to associate the specified value with the specified key in a HashMap;

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;
import java.util.Map;

public class EX1 {

    public static void main(String[] args) {

        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, " Red  " );
        map.put( 2, " Green" );
        map.put( 3, " Black" );
        map.put( 4, " White" );
        map.put( 5, " Blue " );

        for (Map.Entry x:map.entrySet()){
            System.out.println(x.getKey()+ " " + x.getValue());
        }
    }

}
