// Write a Java program to remove all of the mappings from a map.

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX4 {

    public static void main(String[] args) {

        HashMap<Integer,String> mapa = new HashMap<>();
        mapa.put( 1, "Red" );
        mapa.put( 2, "Green" );
        mapa.put( 3, "Black" );
        mapa.put( 4, "White" );
        mapa.put( 5, "Blue" );

        System.out.println(mapa);

        mapa.clear();

        System.out.println(mapa);
    }
}
