package com.minderaschool.Exercise1;

public class Main {
    public static void main(String[] args) {

        Author author=new Author("Mindera School","teste@gmail.com",'m');
        Book book=new Book("os maias",author,21.5);

        System.out.println("O Livro: " + book.getName());
        System.out.println("Preço: " + book.getPrice() + " € ");
        book.printAuthorData();
    }
}
