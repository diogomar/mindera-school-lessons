package com.minderaschool.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Begin {
    private static final String path = "Text";

    public static void main(String[] args) throws IOException {
        File file = new File( path );
        long begin = System.currentTimeMillis();
        BufferedWriter writer = new BufferedWriter( new FileWriter( file ) );
        writer.write( "Arquivo gravado em : " + new SimpleDateFormat().format( new Date() ) );
        writer.newLine();
        writer.write( "Caminho da gravação: " + path );
        writer.newLine();
        long end = System.currentTimeMillis();
        writer.write( "Tempo de gravação: " + (end - begin) + "ms." );
        //Criando o conteúdo do arquivo
        writer.flush();
        //Fechando conexão e escrita do arquivo.
        writer.close();
        System.out.println( "Arquivo gravado em: " + path );
    }
}










