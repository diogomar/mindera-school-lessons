// Write a Java program to test if a map contains a mapping for the specified key.

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX7 {

    public static void main(String[] args) {

        HashMap <String, Integer> mapa = new HashMap<>();
        mapa.put("Red", 1);
        mapa.put("Green", 2);
        mapa.put("Black", 3);
        mapa.put("White", 4);
        mapa.put("Blue", 5);

        // print the map
        System.out.println(" The Original Map : " + mapa);

        System.out.println(" 1. Is Key 'Green' exists? ");

        if(mapa.containsKey(" Green ")){
            //key exists
            System.out.println(" Yes! ");
        }else{
            //key does not exists
            System.out.println(" No! ");
        }

        System.out.println("\n2. Is key 'Orange' exists?");
        if ( mapa.containsKey(" Orange ")){
            //key exists
            System.out.println("Yes! ");
        }else {
            //key does not exists
            System.out.println(" No! ");
        }
    }

}
