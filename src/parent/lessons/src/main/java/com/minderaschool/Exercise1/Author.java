package com.minderaschool.Exercise1;

public class Author  {

    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        if (gender == 'm') {
            return "masculino";
        }
        else if (gender == 'f') {
            return "feminino";
        }
        return "indefenido";
    }
}

