package com.minderaschool.inheritance;

public class Main {
    public static void main(String[] args) {

        Figure figura1 = new Figure();
        System.out.println(figura1.area());
        figura1.WhoAmI();

        Figure figura2 = new Rectangule(5,5);
        System.out.println(figura2.area());
        figura2.WhoAmI();

        Figure figura3 = new Square(5);
        System.out.println(figura3.area());
        figura3.WhoAmI();

        Figure figura4 = new Circle(5);
        System.out.println(figura4.area());
        figura4.WhoAmI();

        Figure figura5 = new Triangule(5,4);
        System.out.println(figura5.area());
        figura5.WhoAmI();

    }
}
