package com.minderaschool.inheritance2;

public class Rectangule extends Figure {



    public Rectangule(double sideA, double sideB) {
        super.areaValue = sideA * sideB;
        super.iAm = "Rectangule";
    }

    public double area() {
        return super.areaValue;
    }

    public void WhoAmI() {
        System.out.println(super.iAm);
    }

    public Rectangule() {
    }

}
