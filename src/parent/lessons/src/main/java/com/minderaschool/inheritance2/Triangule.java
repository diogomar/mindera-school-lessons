package com.minderaschool.inheritance2;

public class Triangule extends Figure {

    public Triangule(double heigth, double weigth) {
        super.areaValue = heigth * weigth / 2;
        super.iAm = "Triangule";
    }

    public double area() {
        return super.areaValue;
    }

    public void WhoAmI() {
        System.out.println(super.iAm);
    }
}
