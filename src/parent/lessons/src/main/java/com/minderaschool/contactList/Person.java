package com.minderaschool.contactList;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private String name;

    private List<Contact> contact = new ArrayList<>();

    public Person(String name, List<Contact> contacts) {
        this.name = name;
        this.contact = contacts;
    }

    public Person(String name, Contact contact) {
        this.name = name;
        this.contact.add(contact);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContact() {
        return this.contact;
    }

    public void setContact(List<Contact> contact) {
        this.contact = contact;
    }
}


