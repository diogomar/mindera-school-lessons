// Write a Java program to count the number of key-value (size) mappings in a map;

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX2 {

    public static void main(String[] args) {

        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put( 1, "Red" );
        mapa.put( 2, "Green" );
        mapa.put( 3, "Black" );
        mapa.put( 4, "White" );
        mapa.put( 5, "Blue" );

        System.out.println( "the size of the hashmap is: " + mapa.size() );
    }
}
