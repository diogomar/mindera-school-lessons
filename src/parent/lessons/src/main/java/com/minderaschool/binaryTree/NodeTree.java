package com.minderaschool.binaryTree;

public class NodeTree {

    private NodeTree left;
    private NodeTree right;
    char element;

    public NodeTree(char element) {
        this.element = element;
        left = right = null;
    }

    public NodeTree getLeft() {
        return left;
    }

    public NodeTree getRight() {
        return right;
    }

    public char getElement() {
        return element;
    }

    public void setLeft(NodeTree left) {
        this.left = left;
    }

    public void setRight(NodeTree right) {
        this.right = right;
    }

    public void setElement(char element) {
        this.element = element;
    }
}


