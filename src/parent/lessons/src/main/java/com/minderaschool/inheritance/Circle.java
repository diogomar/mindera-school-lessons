package com.minderaschool.inheritance;

public class Circle extends Figure {

    public Circle (double r) {
        super.areaValue = Math.PI * (r*r);
        super.IAm = "Circle";
    }

    public double area() {
        return areaValue;
    }

    public void WhoAmI() {
        System.out.println(IAm);
    }
}
