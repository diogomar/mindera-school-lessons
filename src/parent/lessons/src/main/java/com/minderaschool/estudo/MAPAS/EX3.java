// Write a Java program to copy all of the mappings from the specified map to another map;

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX3 {

    public static void main(String[] args) {

        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put( 1, "Red  " );
        mapa.put( 2, "Green" );
        mapa.put( 3, "Black" );
        mapa.put( 4, "White" );
        mapa.put( 5, "Blue " );

        HashMap <Integer,String> mapa2 = new HashMap<>();

        mapa2.putAll(mapa);

        System.out.println(mapa2);
    }
}
