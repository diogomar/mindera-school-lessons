package com.minderaschool.bank;
import java.util.Scanner;

public class BankApplication {

    public  static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("introduza um valor para abrir conta ");
        Account client1 = new Account(scanner.nextDouble());
        System.out.println("introduza um valor para abrir a segunda conta ");
        Account client2 = new Account(scanner.nextDouble());
        System.out.println("introduza o valor para transferir para o cliente 2");
        client1.transfer(scanner.nextDouble(), client2);
        System.out.println("1:" + client1.getBalance() + "\n" + "2:" + client2.getBalance());
        System.out.println("introduza o valor para transferir para o cliente 1");
        client2.transfer(scanner.nextDouble(), client1);
        System.out.println("1:" + client1.getBalance() + "\n" +  "2:" + client2.getBalance());
        scanner.close();
    }
}

