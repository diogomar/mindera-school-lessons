package com.minderaschool.inheritance2;

public class Figure {

    double areaValue = 0.0;
    String iAm = "Figure";

    public double area() {
        return areaValue;
    }

    public void WhoAmI() {
        System.out.println(iAm);
    }
}
