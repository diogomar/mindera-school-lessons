package com.minderaschool.inheritance;

public class Triangule extends Figure {

    public Triangule(double h, double w) {
        super.areaValue = h * w / 2;
        super.IAm = "Triangule";
    }

    public double area() {
        return areaValue;
    }

    public void WhoAmI() {
        System.out.println(IAm);
    }

}
