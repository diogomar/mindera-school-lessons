package com.minderaschool.carSimulator;

public class Door {
    private boolean isOpen;


    public void open() {
        this.isOpen = true;
        System.out.println("Door open");
    }

    public void close() {
        this.isOpen = false;
        System.out.println("Door close");
    }

    public boolean isOpen() {

        return isOpen;
    }



}
