// Write a Java program to check whether a map contains key-value mappings (empty) or not

package com.minderaschool.estudo.MAPAS;

import java.util.HashMap;

public class EX5 {

    public static void main(String[] args) {

        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put( 1, " Red  " );
        mapa.put( 2, " Green" );
        mapa.put( 3, " Black" );
        mapa.put( 4, " White" );
        mapa.put( 5, " Blue " );

        boolean result = mapa.isEmpty();

        System.out.println( "hashmap is empty? \nIt´s " + result );

        mapa.clear();

        System.out.println();

        result = mapa.isEmpty();

        System.out.println( "Hashmap Is Empty? \nIt`s " + result );
    }


}
