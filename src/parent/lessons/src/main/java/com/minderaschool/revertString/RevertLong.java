package com.minderaschool.revertString;

public class RevertLong {

    static long number = (12579);

    public static void main(String[] args) {

        System.out.println( numbers( number , 0) );
    }


    public static long numbers(long number, int iterations) {
        if ( iterations == 3 ) {
            return 0;
        } else {

            return number % 10 + numbers( number / 10 , iterations+1);
        }


    }
}
