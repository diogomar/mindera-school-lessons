package com.minderaschool.inheritance;

public class Figure {

    double areaValue = 0.0;
    String IAm = "Figure";

    public double area() {
        return areaValue;
    }

    public void WhoAmI() {
        System.out.println(IAm);
    }
}
