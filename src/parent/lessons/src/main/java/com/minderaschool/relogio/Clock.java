package com.minderaschool.relogio;

public class Clock {

    private int hours;
    private int minutes;
    private int seconds;

    public Clock(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;

    }

    public Clock() {
        this.hours = 12;
        this.minutes = 0;
        this.seconds = 0;
    }

    public Clock(int seconds) {
        this.hours = seconds / 3600;
        seconds -= this.hours * 3600;
        this.minutes = seconds / 60;
        seconds -= this.minutes * 60;
        this.seconds = seconds / seconds;
    }

    public void setClock(int seconds) {
        this.hours = seconds / 3600;
        seconds -= this.hours * 3600;
        this.minutes = seconds / 60;
        seconds -= this.minutes * 60;
        this.seconds = seconds / seconds;
    }

    public int getHours(int hours) {
        return this.hours;
    }

    public int getMinutes(int minutes) {
        return this.minutes;
    }

    public int getSeconds(int seconds) {
        return this.seconds;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public void tick() {
        if (seconds + 1 > 59) {
            if (minutes + 1 > 59) {
                if (hours + 1 > 23) {
                    hours = 0;
                    minutes = 0;
                    seconds = 0;
                } else {
                    hours++;
                    minutes = 0;
                    seconds = 0;
                }


            } else {
                minutes++;
                seconds = 0;
            }
        } else {
            seconds++;
        }
    }

    public void addClock(Clock relogio) {
        int hours = relogio.hours * 3600;

        int minutes = relogio.minutes * 60;

        int seconds = relogio.seconds;
        int total = hours + minutes + seconds;
        for (int x = 0; x < total; x++) {
            tick();
        }
    }

    public String toString() {
        return this.hours + ":" + this.minutes + ":" + this.seconds;
    }


}
