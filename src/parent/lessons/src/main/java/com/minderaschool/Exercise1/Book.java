package com.minderaschool.Exercise1;

public class Book {
    private String name;
    public Author author;
    public double price;


    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void printAuthorData(){
        System.out.println("O autor chama-se " + author.getName() + " tem o sexo: " +author.getGender()  + " e tem o email: " +author.getEmail() );
    }
}
