package com.minderaschool.utils;

public class Logger {

    private String name;

    public Logger() {
        this("Default");
    }

    public Logger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void info(String msg) {
        System.out.printf("%s - Info - %s \n", this.name, msg);
    }

    public void error(String msg) {
        System.out.printf("%s - Error - %s \n", this.name, msg);
    }

    public void debug(String msg) {
        System.out.printf("%s - Debug - %s \n", this.name, msg);
    }
}