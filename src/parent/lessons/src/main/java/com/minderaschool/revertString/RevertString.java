package com.minderaschool.revertString;

public class RevertString {

    static String word = "ABCDEF";

    public static void main(String[] args) {
        System.out.println( words( word ) );
    }

    public static String words(String word) {


        if ( word.isEmpty() ) {
            return "";
        } else {
            return words( word.substring( 1 ) ) + word.charAt( 0 );
        }

    }
}
