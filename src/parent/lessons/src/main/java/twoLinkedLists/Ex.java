package twoLinkedLists;


import java.util.LinkedList;
import java.util.Stack;

public class Ex {

    public static void main(String[] args) {

        LinkedList<Integer> lista = new LinkedList<Integer>();

        lista.add( 1 );
        lista.add( 2 );
        lista.add( 3 );

        System.out.println( lista );

        System.out.println();

        LinkedList<Integer> listaTwo = new LinkedList<Integer>();

        listaTwo.add( 4 );
        listaTwo.add( 5 );
        listaTwo.add( 6 );

        System.out.println( listaTwo );

        System.out.println();

        LinkedList<Integer> total = new LinkedList<Integer>();

        Stack<Integer> stack = new Stack<>();

        for (int i = lista.size()-1; i >= 0; i--) {
            stack.push( lista.get( i ) + listaTwo.get( i ) );
        }

        while (!stack.empty()) {
            total.add( stack.pop() );

        }

        System.out.println( total );
    }

}
