package Exercise2;

public class Main2 {

    public static void main (String[]args){
        Student person1 =new Student("minder","rua goncalo cristovao");
        Teacher person2 = new Teacher("master","rua das couves");

        System.out.println("\nStudent: " + person1);
        System.out.println("Teacher: " + person2);

        person1.addCourseGrade("matematica", 15);
        person1.addCourseGrade("portugues", 10);
        person1.addCourseGrade("ingles", 5);

        //System.out.println(person1.getAverageGrades());

        person2.addCourse("PSI");
        person2.addCourse("MAT");
        person2.addCourse("ING");

        //person2.printCourses(); // ADICIONEI PARA VER SE ESTA A FUNCIONAR

        person2.removeCourse("MAT");

        //person2.printCourses();
    }
}
