package Exercise2;

public class Teacher extends Person {

    private int numCouses;
    private String[] courses = new String[5];

    public Teacher(String name, String adress) {
        super(name, adress);
    }

    public boolean addCourse(String course) {
        return true;
    }

    public boolean removeCourse(String course){
        return false;
    }

    public String toString(){
        return "Teacher: " + getName() + " " + getAdress();
    }
}
