package ficheiros;

import java.util.List;

public class Main {

    public static void main(String[] args) { //psvm
        Exercicio1 ex1 = new Exercicio1();

        List<String> list = ex1.readFile();

        printList( list );


    }

    public static void printList(List<String> list) {
        for (String linha : list) {
            System.out.println( linha );
        }
    }
}
