package ficheiros;

import java.util.HashMap;

public class metric {

    String metricName;
    String metricValue;
    String type;
    HashMap<String, String> labels;

    public metric(String metricName, String metricValue, String type, HashMap<String, String> labels) {
        this.metricName = metricName;
        this.metricValue = metricValue;
        this.type = type;
        this.labels = new HashMap<>();
    }

    public String getMetricName() {
        return metricName;
    }

    public String getMetricValue() {
        return metricValue;
    }

    public String getType() {
        return type;
    }

    public HashMap<String, String> getLabels() {
        return labels;
    }
}

